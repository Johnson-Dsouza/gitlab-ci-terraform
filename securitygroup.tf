resource "aws_security_group" "mywebsecurity" {
  name        = "web_security"
  description = "Allow HTTP, SSH"
  vpc_id      = aws_vpc.ownvpc.id


  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  tags = {
    Name = "Terraform-Security-Group"
  }
}

