// public subnet

resource "aws_subnet" "public" {
  vpc_id            = aws_vpc.ownvpc.id
  cidr_block        = "192.168.0.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    "Name" = "Terraform-Public-Subnet"
  }
}

// private subnet

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.ownvpc.id
  cidr_block        = "192.168.1.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    "Name" = "Terraform-Private-Subnet"
  }
}

