resource "aws_internet_gateway" "mygateway" {
  vpc_id = aws_vpc.ownvpc.id

  tags = {
    "Name" = "Terraform-Internet-Gateway"
  }
}

