resource "aws_flow_log" "vpc_logs" {
  log_destination      = aws_s3_bucket.s3.arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_vpc.ownvpc.id
}

resource "aws_kms_key" "kms_key" {
  enable_key_rotation = true
}

resource "aws_s3_bucket" "s3" {
  bucket = "codemancers-production-terraform-state-testing"

}

resource "aws_s3_bucket_versioning" "s3_versioning" {
  bucket = aws_s3_bucket.s3.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_logging" "bucket_logging" {
  bucket = aws_s3_bucket.s3.id

  target_bucket = aws_s3_bucket.s3.id
  target_prefix = "log/"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "example" {
  bucket = aws_s3_bucket.s3.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.kms_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "s3_access_blocks" {
  bucket              = aws_s3_bucket.s3.id
  block_public_policy = true
  ignore_public_acls  = true
  block_public_acls   = true

  restrict_public_buckets = true
}

