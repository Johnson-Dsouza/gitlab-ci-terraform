variable "aws_region" {
  type        = string
  description = "aws region"
}

variable "ec2_ami" {
  type        = string
  description = "AMI"
}

variable "ec2_type" {
  type        = string
  description = "Instance type"
}

