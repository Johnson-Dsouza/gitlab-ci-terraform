# GitLab-AWS-Terraform
**To setup the GitLab CICD pipeline for terraform:**

**Step 1:** Grab the source code from the gitlab repository.

**Step 2:** Then install pre-commit hooks on terminal -> [Install pre-commit](https://pre-commit.com/) -> Install third-party tools like tflint, tfsec etc on terminal -> Install the pre-commit hook, simply run:

`pre-commit install`

**Step 3:** Add Access keys (access key ID and secret access key) to GitLab -> Setting -> CI/CD -> Variables -> Add access key, secret access key.

**Step 4:** Optional if using own runner in GitLab -> Setting -> CI/CD -> Runners -> [Install GitLab Runner and ensure it's running](https://docs.gitlab.com/runner/install/) -> Register the runner with this URL: https://gitlab.com/ -> And this registration token -> After getting runner in Available specific runners -> Disable shared runners for this project.
